using UnityEngine;
using UnityEngine.AI;


public class PatrolState : NPCstateBase
{
    int TurnToDegree;
    MonoBehaviour[] markers;
    MonoBehaviour targetMono;
    Transform Target;
    float distance;
    public override void EnterState(NPCstateManager npc)
    {
        npc.GetComponent<NavMeshAgent>().speed = npc.GetComponent<NPC_ATTRIBUTES>().speed;
        markers = GameObject.FindObjectsOfType<PatrolMarker>();
        TurnToDegree= Random.Range(0,12);
        Debug.Log("Enter Patrol Mode");
        targetMono = GameObject.FindObjectOfType<PlayerController>();
        Target = targetMono.gameObject.transform;
       // Debug.Log(TurnToDegree);
    }

    public override void UpdateState(NPCstateManager npc)
    {
       // Debug.Log("Walking to destinacion");
        distance = Vector3.Distance(Target.position, npc.transform.position);
        if (npc.GetComponent<NPC_ATTRIBUTES>().mele == true)
        {
            if (distance > npc.GetComponent<NPC_ATTRIBUTES>().range)
            {
                npc.transform.GetComponent<NavMeshAgent>().SetDestination(new Vector3(markers[TurnToDegree].transform.position.x, markers[TurnToDegree].transform.position.y, markers[TurnToDegree].transform.position.z));
                npc.GetComponent<Animator>().Play("walk");
                if (npc.transform.GetComponent<NavMeshAgent>().remainingDistance < 1)
                {
                    npc.GetComponent<Animator>().Play("idle");
                    npc.SwitchState(npc.idleState);
                }
            }
            else
            {
                npc.SwitchState(npc.chasingState);
            }
        }
        if (npc.GetComponent<NPC_ATTRIBUTES>().Shooter == true)
        {
            if(distance<npc.GetComponent<NPC_ATTRIBUTES>().range)
            {
                npc.SwitchState(npc.takeCoverState);
            }
        }
    }

    public override void OnCollisionState(NPCstateManager npc)
    {

    }
    
}
