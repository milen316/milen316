using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCstateManager : MonoBehaviour
{
    public NPCstateBase currentState;
    public IdleState idleState = new IdleState();
    public PatrolState patrolState = new PatrolState();
    public AttackState attackState = new AttackState();
    public ChasingState chasingState = new ChasingState();
    public WaitingAfterAttack waitingAfterAttack = new WaitingAfterAttack();
    public TakeCoverState takeCoverState = new TakeCoverState();
    public ShooterCoverAttack shooterCoverAttack = new ShooterCoverAttack();
    // Start is called before the first frame update
    void Start()
    {
        currentState = idleState;
        currentState.EnterState(this);
    }

    // Update is called once per frame
    void Update()
    {
        currentState.UpdateState(this);
    }
    public void SwitchState(NPCstateBase state)
    {
        currentState = state;
        state.EnterState(this);
    }

}
