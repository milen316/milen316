using UnityEngine;

public class IdleState : NPCstateBase
{
    float timer;
    float seconds = 2;
    public override void EnterState(NPCstateManager npc)
    {
        timer = 0;
        Debug.Log("Hello from the IdleState, Method EnterState");
    }

    public override void UpdateState(NPCstateManager npc)
    {
        if(timer<seconds)
        {
            timer += 1 * Time.deltaTime;
        }else
        {
            
            npc.SwitchState(npc.patrolState);
        }
       // Debug.Log("Hello from the IdleState, Method UpdateState");
    }

    public override void OnCollisionState(NPCstateManager npc)
    {

    }
}
