using UnityEngine;
using UnityEngine.AI;

public class ChasingState : NPCstateBase
{
    // Start is called before the first frame update
    float DistanceToAttack;
    MonoBehaviour TargetMono;
    Transform Target;
  
    public override void EnterState(NPCstateManager npc)
    {
        npc.GetComponent<NavMeshAgent>().speed = npc.GetComponent<NPC_ATTRIBUTES>().runningSpeed;
        TargetMono = GameObject.FindObjectOfType<PlayerController>();
        Target = TargetMono.transform;
        Debug.Log("Chasing mode ON");
        
        
    }

    public override void UpdateState(NPCstateManager npc)
    {
        DistanceToAttack = Vector3.Distance(Target.position, npc.transform.position);
        if (DistanceToAttack > 1.5f && npc.GetComponent<NPC_ATTRIBUTES>().attack == false)
        {
            npc.GetComponent<Animator>().Play("run");
            npc.transform.GetComponent<NavMeshAgent>().SetDestination(new Vector3(Target.position.x, Target.position.y, Target.position.z));
        }
        else
        {
            npc.SwitchState(npc.attackState);
           
        }
    }

    public override void OnCollisionState(NPCstateManager npc)
    {

    }
}
