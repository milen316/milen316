using UnityEngine;

public class WaitingAfterAttack : NPCstateBase
{
    float timer;
    float seconds = 1;
    float DistanceToAttack;
    MonoBehaviour TargetMono;
    Transform Target;
    // Start is called before the first frame update
    public override void EnterState(NPCstateManager npc)
    {
        TargetMono = GameObject.FindObjectOfType<PlayerController>();
        Target = TargetMono.transform;
        DistanceToAttack = Vector3.Distance(Target.position, npc.transform.position);
        timer = 0;
    }

    public override void UpdateState(NPCstateManager npc)
    {
        DistanceToAttack = Vector3.Distance(Target.position, npc.transform.position);
        timer+= 1 * Time.deltaTime;
        if(timer>0.75)
        {
            npc.GetComponent<Animator>().Play("idle");
        }    
        if (timer >= seconds)
        {
            if (DistanceToAttack < 1.5f)
            {
                npc.SwitchState(npc.attackState);
            }
            else
            {
                npc.SwitchState(npc.chasingState);
            }
        }
    
    }

    public override void OnCollisionState(NPCstateManager npc)
    {

    }
}
