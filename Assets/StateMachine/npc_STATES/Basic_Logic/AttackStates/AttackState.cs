using UnityEngine;

public class AttackState : NPCstateBase
{
    float DistanceToAttack;
    MonoBehaviour TargetMono;
    Transform Target;
    int damage = 30;
    bool attacking = false;
   

    float timerPerAttack;
    public override void EnterState(NPCstateManager npc)
    {
        TargetMono = GameObject.FindObjectOfType<PlayerController>();
        Target = TargetMono.transform;
        Debug.Log("Attack Mode ON");
        DistanceToAttack = Vector3.Distance(Target.position, npc.transform.position);
        if (npc.GetComponent<NPC_ATTRIBUTES>().attack == false)
        {
            npc.GetComponent<NPC_ATTRIBUTES>().iniCoroutine();
            npc.SwitchState(npc.waitingAfterAttack);
        }
        else
        {

            npc.SwitchState(npc.waitingAfterAttack);

        }


    }

    public override void UpdateState(NPCstateManager npc)
    { 

        // DistanceToAttack = Vector3.Distance(Target.position, npc.transform.position);

      
    }

    public override void OnCollisionState(NPCstateManager npc)
    {

    }

    
}
