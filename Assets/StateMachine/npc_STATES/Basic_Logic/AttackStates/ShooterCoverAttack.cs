using UnityEngine;
using UnityEngine.AI;

public class ShooterCoverAttack : NPCstateBase 
{
    float distance;
    Transform target;
   
    public override void EnterState(NPCstateManager npc)
    {
        target = MonoBehaviour.FindObjectOfType<PlayerController>().transform;
        npc.GetComponent<Animator>().Play("duck");

        Debug.Log("Covering ... Ducking...");
    }

    public override void UpdateState(NPCstateManager npc)
    {
      if(npc.GetComponent<NPC_ATTRIBUTES>().attack == false)
        {
            npc.GetComponent<NPC_ATTRIBUTES>().startCoveringAndShooting();
        }
      
            
        
    }

    public override void OnCollisionState(NPCstateManager npc)
    {

    }
}
