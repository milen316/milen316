using UnityEngine;
using UnityEngine.AI;

public class TakeCoverState : NPCstateBase
{
    Transform placetoHide;
    float distance;
    public override void EnterState(NPCstateManager npc)
    {
        //Not Working almost too slow figuring out for current Position  //   MonoBehaviour.FindObjectOfType<TestDeletableScript>().gameObject.transform.position = npc.transform.position;
        npc.GetComponent<NavMeshAgent>().speed = npc.GetComponent<NPC_ATTRIBUTES>().runningSpeed;
        placetoHide = MonoBehaviour.FindObjectOfType<TestDeletableScript>().currentTargetForCover;
        GameObject.FindObjectOfType<TestDeletableScript>().refreshCoverPoints();
        Debug.Log("Enter Cover State");
        distance = Vector3.Distance(placetoHide.position, npc.transform.position);
       
    }

    public override void UpdateState(NPCstateManager npc)
    {
      //  distance = Vector3.Distance(placetoHide.position, npc.transform.position);
        distance = Vector3.Distance(placetoHide.position, npc.transform.position);
        //placetoHide = MonoBehaviour.FindObjectOfType<TestDeletableScript>().currentTargetForCover;
        npc.transform.GetComponent<NavMeshAgent>().SetDestination(new Vector3(placetoHide.position.x, placetoHide.position.y, placetoHide.position.z));
        npc.GetComponent<Animator>().Play("run");
        //Debug.Log(distance);
        if(distance<=1.2f)
        {
            npc.SwitchState(npc.shooterCoverAttack);
            
        }
    }

    public override void OnCollisionState(NPCstateManager npc)
    {

    }
}
