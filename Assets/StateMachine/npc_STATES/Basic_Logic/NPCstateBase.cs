using UnityEngine;

public abstract class NPCstateBase
{
   public abstract void EnterState(NPCstateManager npc);

   public abstract void UpdateState(NPCstateManager npc);

   public abstract void OnCollisionState(NPCstateManager npc);

    
}
