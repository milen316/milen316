using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPC_ATTRIBUTES : MonoBehaviour
{
    //TODO
    public bool mele;
    public bool Shooter;
    public int health;
    public int damage;
    public float speed;
    public float runningSpeed;
    public float range;
    public float DistanceToTarget;
    public GameObject[] weapons;
    public bool attack;
    public float AttackSpeed;
    public Transform Target;



    public bool isAlive;
    public bool lookAt;

    // Start is called before the first frame update
    void Start()
    {
        MonoBehaviour targetControllerScript = FindObjectOfType<PlayerController>();
        Target = targetControllerScript.gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            isAlive = false;
        }
        if (isAlive)
        {
            DistanceToTarget = Vector3.Distance(Target.transform.position, transform.position);
        }
        else
        {
            GetComponent<Animator>().Play("Die");
        }
        if(lookAt == true)
        {
            Vector3 targetPostition = new Vector3(Target.position.x,
                                       this.transform.position.y,
                                       Target.position.z);
            this.transform.LookAt(targetPostition);
        }

    }
    public IEnumerator AttackNumerator()
    {
        Debug.Log("AttackIenumeroator Executes times");
        attack = true;
        GetComponent<Animator>().Play("attack");
        float agentSpeed = GetComponent<NavMeshAgent>().speed;
        GetComponent<NavMeshAgent>().speed = 0;
        yield return new WaitForSeconds(AttackSpeed);
        GetComponent<NavMeshAgent>().speed = agentSpeed;
        attack = false;
    }
    public void iniCoroutine()
    {
        StartCoroutine(AttackNumerator());
    }
  
        public IEnumerator ShowAndHideCover()
    {
        attack = true;
        float showhide = Random.Range(1.5f,3f);
        yield return new WaitForSeconds(showhide);
        lookAt = true;
        GetComponent<Animator>().Play("shooting");
        yield return new WaitForSeconds(2f);
        lookAt = false;
        GetComponent<Animator>().Play("duck");
        yield return new WaitForSeconds(showhide);
        attack = false;
    }
    public void startCoveringAndShooting()
    {
        StartCoroutine(ShowAndHideCover());
    }
}
