using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    public float speed;
    public Transform Target;
    public GameObject Spine;
    public GameObject PlayerCamera;
    public GameObject AimCamera;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetPostition = new Vector3(Target.position.x,
                                       this.transform.position.y,
                                       Target.position.z);
        this.transform.LookAt(targetPostition);
        
        float x = Input.GetAxisRaw("Horizontal");
        float z = Input.GetAxisRaw("Vertical");
        Vector3 moveBy = transform.right * x + transform.forward * z;
        rb.MovePosition(transform.position + moveBy.normalized * speed * Time.deltaTime);
        if (z > 0)
        {
            GetComponent<Animator>().SetBool("walk", true);
        } else
        {
            GetComponent<Animator>().SetBool("walk", false);
        }
        if (z < 0)
        {
            GetComponent<Animator>().SetBool("backward", true);
        }
        else
        {
            GetComponent<Animator>().SetBool("backward", false);
        }

        AimToTarget();
    }
    public void AimToTarget()
    {
          if (Input.GetKeyDown(KeyCode.Mouse1))
        {

            
            PlayerCamera.SetActive(false);
            AimCamera.SetActive(true);
            GetComponent<Animator>().SetBool("aiming", true);
            //Allow time for the camera to blend before enabling the UI
            // StartCoroutine(ShowReticle());
        }
        else if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            PlayerCamera.SetActive(true);
            AimCamera.SetActive(false);
            GetComponent<Animator>().SetBool("aiming", false);
        }
      
        
    }
}
