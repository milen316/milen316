using UnityEngine;



public class aimScript : MonoBehaviour
{
    public Animator anim;
    public float speed = 7.5f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public Transform playerCameraParent;
    public float lookSpeed = 2.0f;
    public float lookXLimit = 60.0f;

    CharacterController characterController;
    Vector3 moveDirection = Vector3.zero;
    Vector2 rotation = Vector2.zero;
   public  float roty = 0;
    [HideInInspector]
    public bool canMove = true;

    void Start()
    {
      
        rotation.y = transform.eulerAngles.y;
    }

    void Update()
    {
        
        if (rotation.y< roty)
        {
           anim.SetBool("RotLeft",true);
        }
        else
        {
            anim.SetBool("RotLeft", false);
        }
        if (rotation.y > roty)
        {
            anim.SetBool("RotRight", true);
        }
        else
        {
            anim.SetBool("RotRight", false);
        }
      //  Debug.Log(rotation.x + "x     y" + rotation.y);
        roty= rotation.y;
        //Debug.Log(roty);

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller
     

        // Player and Camera rotation
        if (canMove)
        {
            rotation.y += Input.GetAxis("Mouse X") * lookSpeed;
            rotation.x += -Input.GetAxis("Mouse Y") * lookSpeed;
            rotation.x = Mathf.Clamp(rotation.x, -lookXLimit, lookXLimit);
            playerCameraParent.localRotation = Quaternion.Euler(rotation.x, 0, 0);
            transform.eulerAngles = new Vector2(rotation.x, rotation.y);
        }
    }
}