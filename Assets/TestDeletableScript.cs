using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;



public class TestDeletableScript : MonoBehaviour
{
    [System.Serializable]
    public struct TakeCoverObjects
    {
        public bool Taken;
        public bool Initialize;
        public GameObject CoverObject;
        public float distance;
    }
    public float mineDistance;
    public GameObject[] AllCovers;
    public int FreeCovers;
    public TakeCoverObjects[] cover;
    public Transform currentTargetForCover;
    // Start is called before the first frame update
    void Start()
    {
        AllCovers = GameObject.FindGameObjectsWithTag("coverPlace");
        cover = new TakeCoverObjects[GameObject.FindGameObjectsWithTag("coverPlace").Length];
        FreeCovers = cover.Length;

        for (int i = 0; i < cover.Length; i++)
        {
            if (cover[i].Initialize ==false)
            {
                cover[i].CoverObject = AllCovers[i];
                cover[i].Initialize = true;
                cover[i].distance = Vector3.Distance(cover[i].CoverObject.transform.position, gameObject.transform.position);
            }
           
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < cover.Length; i++)
        {
            
                cover[i].distance = Vector3.Distance(cover[i].CoverObject.transform.position, gameObject.transform.position);
            

        }
   
       var sorted = cover.OrderBy(p => p.distance).Where(p => p.Taken == false).ToArray();
        for (int i = 0; i < cover.Length; i++)
        {
            currentTargetForCover = sorted[0].CoverObject.transform;
        

        }
      
    }
    public void refreshCoverPoints()
    {
        var sorted = cover.OrderBy(p => p.distance).Where(p => p.Taken == false).ToArray();
        for (int i = 0; i < cover.Length; i++)
        {
            // cover[int.Parse(sorted[0].ToString())].Taken = true;
            if(cover[i].distance == sorted[0].distance)
            {
                cover[i].Taken = true;
            }


        }
    }
}
